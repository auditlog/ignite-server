package com.auditlog.igniteserver;

import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;
import org.apache.ignite.configuration.CacheConfiguration;
import org.apache.ignite.configuration.DataStorageConfiguration;
import org.apache.ignite.configuration.DeploymentMode;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.cache.configuration.FactoryBuilder;
import java.util.UUID;

@Configuration
public class IgniteConfig {


    protected static final String igniteID = UUID.randomUUID().toString();

    @Bean
    public Ignite igniteInstance() {
        IgniteConfiguration cfg = new IgniteConfiguration();
        cfg.setIgniteInstanceName("ignite-node-"+igniteID);
        cfg.setPeerClassLoadingEnabled(false);
        cfg.setDeploymentMode(DeploymentMode.CONTINUOUS);
        cfg.setPeerClassLoadingMissedResourcesCacheSize(0);
        cfg.setFailureDetectionTimeout(10000);
        cfg.setClientFailureDetectionTimeout(10000);
        cfg.setNetworkTimeout(10000);

        //setup persistence
        setupNativePersistence(cfg);

        Ignite ignite = Ignition.start(cfg);
        ignite.cluster().active(true);
        return ignite;
    }

    private void setupNativePersistence( IgniteConfiguration cfg ) {
        DataStorageConfiguration storageCfg = new DataStorageConfiguration();
        storageCfg.getDefaultDataRegionConfiguration().setPersistenceEnabled(true);
//		persistentStoreConfiguration.setPersistentStorePath("./data/store");
        //          persistentStoreConfiguration.setWalArchivePath("./data/walArchive");
//persistentStoreConfiguration.setWalStorePath("./data/walStore");
        cfg.setDataStorageConfiguration(storageCfg);
    }


}
